#!/usr/bin/python

import sys

phylo = {}

with open("snp_phylo.csv") as infile:
	next(infile)
	for line in infile:
		line = line.strip().split("\t")
		snpid = line[1]+line[3].replace("/","")
		phylo[snpid] = line[0]


with open(sys.argv[1]) as snpfile:
	next(snpfile)
	out = []
	lins = []
	for line in snpfile:
		line = line.strip().split("\t")

		if len(list(line[-1])) == 1:

			snpid = line[1] + line[2] + line[-1]

			freq = float(line[6].replace("%",""))

			if snpid in phylo.keys():
				out.append(sys.argv[1] + "," + phylo[snpid] + "," + snpid + "," + str(freq))
				lins.append(phylo[snpid])

	#we need to check that we have at least two different lineages, or sublineages of the same

	diff = []
	for lin in lins:
		raw = lin.split(".")[0]
		if raw not in diff:
			diff.append(raw)

	if len(diff) > 1:
		for l in out:
			print(l+",MI")
	else:

		diff2 = []
		for lin in lins:
			raw = ".".join(lin.split(".")[:2])
			if "." in raw and raw not in diff2:
				diff2.append(raw)

		if len(diff2) > 1:
			for l in out:
				print(l+",PI")
		else:
			print "\n".join(out)