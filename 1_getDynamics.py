#!/usr/bin/python

import sys

patients = {"G018":["SRR3544732","SRR3544718"],
			"G019":["SRR3544724","SRR3544735"],
			"G021":["SRR3544716","SRR3544747"],
			"G023":["SRR3544738","SRR5153132"],
			"G025":["SRR3544733","SRR5153272"],
			"G032":["SRR3544727","SRR5153276"],
			"G036":["SRR3544720","SRR5153273"],
			"G327":["SRR7516397","SRR7516396"],
			"G085":["SRR5153322","SRR7516340"],
			"G033":["SRR3544740","SRR7516345"],
			"G240":["SRR7516457","SRR5152928"],
			"G330":["SRR7516412","SRR7516411"],
			"G324":["SRR7516381","SRR7516380"],
			"G335":["SRR7516409","SRR7516408"],
			"G035":["P-13-S1","P-13-C"],
			"G034":["P-14-S1","P-14-C"],
			"G031":["P-15-S1","P-15-C"],
			"G085":["P-28-S1","P-28-C"],
			"G039":["G-039_Sm","SRR5153601"]}


fasta = open("MTB_ancestor_reference.fasta").read()
ancestor = fasta.replace("\n","").replace(">MTB_anc","")

discard = ['IG_Rv3401_Rv3402c','Rv1435c','Rv2048c','Rv3876','IG_Rv2308_Rv2309c','Rv0020c','IG_Rv0570_Rv0571c','Rv2543','Rv3109','IG_Rv3303c_Rv3304','Rv2931','IG_Rv2980_Rv2981c','IG_Rv3423c_Rv3424c','Rv1267c','Rv3879c','Rv2986c','Rv2020c','IG_Rv1458c_Rv1459c','Rv0090','Rv3453','Rv0071','IG_Rv0126_Rv0127','IG_Rv1682_Rv1683','Rv1907c','Rv0823c','IG_Rv0420c_Rv0421c','Rv1899c','Rv3655c','IG_Rv0490_Rv0491','Rv0176','IG_Rv2673_Rv2674','IG_Rv1506c_Rv1507c','IG_Rv2882c_Rv2883c','Rv3680','Rv3710','IG_Rv2689c_Rv2690c','IG_Rv3680_Rv3681c','Rv0050','Rv1730c','Rv0032','Rv1844c','IG_Rv0035_Rv0036c','IG_Rv0823c_Rv0824c','IG_Rv2905_Rv2906c','IG_Rv2924c_Rv2925c','IG_Rv3646c_Rv3647c','IG_Rv3682_Rv3683','Rv0024','Rv0036c','Rv0063','Rv0134','Rv0405','Rv1793','Rv1883c','Rv1997','Rv2027c','Rv2293c','Rv2319c','Rv2407','Rv2437','Rv2709','Rv2882c','Rv3401','Rv3894c','Rv0045c','Rv1775','Rv2262c','Rv2251','IG_Rv0759c_Rv0760c','IG_Rv2862c_Rv2862A','Rv1551','IG_Rv3443c_Rv3444c','IG_Rv3547_Rv3548c','Rv2880c','IG_Rv2426c_Rv2427c','Rv0892','Rv1198','IG_Rv3775_Rv3776','Rv1699','Rv3911','IG_Rv3202c_Rv3203','IG_Rv3862c_Rv3863','Rv1046c','Rv2339','Rv3847','IG_Rv0001_Rv0002','Rv1469','Rv2923c','IG_Rv1264_Rv1265','IG_Rv1729c_Rv1730c','Rv2561','Rv2975c','Rv2264c','Rv2947c','IG_Rv3053c_Rv3054c','Rv1045','Rv3766','Rv0927c','Rv1125','IG_Rv3060c_Rv3061c','Rv2545','IG_Rv1689_Rv1690','Rv2112c','Rv0750','Rv3172c','Rv2526','IG_Rv0712_Rv0713','Rv2308','Rv2349c','Rv3383c','IG_Rv3747_Rv3748','Rv1402','IG_Rv1829_Rv1830','Rv2700','Rv2544','IG_Rv2945c_Rv2946c','IG_Rv3105c_Rv3106','Rv1371','Rv1197','Rv0174','Rv0176', 'Rv0750','Rv1198','Rv2932','IG_Rv0030_Rv0031', 'Rv0031', 'IG_Rv0031_Rv0032', 'IG_Rv0093c_Rv0094c', 'Rv0094c', 'IG_Rv0094c_Rv0095c', 'Rv0095c', 'IG_Rv0095c_Rv0096', 'Rv0096', 'IG_Rv0096_Rv0097', 'IG_Rv0108c_Rv0109', 'Rv0109', 'IG_Rv0109_Rv0110', 'IG_Rv0123_Rv0124', 'Rv0124', 'IG_Rv0124_Rv0125', 'IG_Rv0150c_Rv0151c', 'Rv0151c', 'IG_Rv0151c_Rv0152c', 'Rv0152c', 'IG_Rv0152c_Rv0153c', 'IG_Rv0158_Rv0159c', 'Rv0159c', 'IG_Rv0159c_Rv0160c', 'Rv0160c', 'IG_Rv0160c_Rv0161', 'IG_Rv0255c_Rv0256c', 'Rv0256c', 'IG_Rv0256c_Rv0257', 'IG_Rv0277c_Rv0278c', 'Rv0278c', 'IG_Rv0278c_Rv0279c', 'Rv0279c', 'IG_Rv0279c_Rv0280', 'Rv0280', 'IG_Rv0280_Rv0281', 'Rv0285', 'IG_Rv0285_Rv0286', 'Rv0286', 'IG_Rv0286_Rv0287', 'IG_Rv0296c_Rv0297', 'Rv0297', 'IG_Rv0297_Rv0298', 'IG_Rv0303_Rv0304c', 'Rv0304c', 'IG_Rv0304c_Rv0305c', 'Rv0305c', 'IG_Rv0305c_Rv0306', 'IG_Rv0334_Rv0335c', 'Rv0335c', 'IG_Rv0335c_Rv0336', 'Rv0336', 'IG_Rv0336_Rv0337c', 'IG_Rv0353_Rv0354c', 'Rv0354c', 'IG_Rv0354c_Rv0355c', 'Rv0355c', 'IG_Rv0355c_Rv0356c', 'IG_Rv0386_Rv0387c', 'Rv0387c', 'IG_Rv0387c_Rv0388c', 'Rv0388c', 'IG_Rv0388c_Rv0389', 'IG_Rv0392c_Rv0393', 'Rv0393', 'IG_Rv0393_Rv0394c', 'IG_Rv0396_Rv0397', 'Rv0397', 'IG_Rv0397_Rv0398c', 'IG_Rv0441c_Rv0442c', 'Rv0442c', 'IG_Rv0442c_Rv0443', 'IG_Rv0452_Rv0453', 'Rv0453', 'IG_Rv0453_Rv0454', 'IG_Rv0514_Rv0515', 'Rv0515', 'IG_Rv0531_Rv0532', 'Rv0532', 'IG_Rv0577_Rv0578c', 'Rv0578c', 'IG_Rv0578c_Rv0579', 'IG_Rv0740_Rv0741', 'Rv0741', 'IG_Rv0741_Rv0742', 'Rv0742', 'IG_Rv0742_Rv0743c', 'IG_Rv0745_Rv0746', 'Rv0746', 'IG_Rv0746_Rv0747', 'Rv0747', 'IG_Rv0747_Rv0748', 'IG_Rv0753c_Rv0754', 'Rv0754', 'IG_Rv0754_Rv0755c', 'Rv0755c', 'IG_Rv0755c_Rv0755A', 'Rv0755A', 'IG_Rv0755A_Rv0756c', 'IG_Rv0794c_Rv0795', 'Rv0795', 'Rv0796', 'IG_Rv0796_Rv0797', 'Rv0797', 'IG_Rv0831c_Rv0832', 'Rv0832', 'Rv0833', 'IG_Rv0833_Rv0834c', 'Rv0834c', 'IG_Rv0834c_Rv0835', 'Rv0850', 'IG_Rv0871_Rv0872c', 'Rv0872c', 'IG_Rv0872c_Rv0873', 'IG_Rv0877_Rv0878c', 'Rv0878c', 'IG_Rv0878c_Rv0879c', 'IG_Rv0914c_Rv0915c', 'Rv0915c', 'IG_Rv0915c_Rv0916c', 'Rv0916c', 'IG_Rv0916c_Rv0917', 'IG_Rv0919_Rv0920c', 'Rv0920c', 'IG_Rv0920c_Rv0921', 'Rv0921', 'Rv0922', 'IG_Rv0922_Rv0923c', 'IG_Rv0976c_Rv0977', 'Rv0977', 'IG_Rv0977_Rv0978c', 'Rv0978c', 'IG_Rv0978c_Rv0979c', 'IG_Rv0979A_Rv0980c', 'Rv0980c', 'IG_Rv0980c_Rv0981', 'IG_Rv1033c_Rv1034c', 'Rv1034c', 'IG_Rv1034c_Rv1035c', 'Rv1035c', 'IG_Rv1035c_Rv1036c', 'IG_Rv1038c_Rv1039c', 'Rv1039c', 'IG_Rv1039c_Rv1040c', 'Rv1040c', 'IG_Rv1040c_Rv1041c', 'Rv1041c', 'Rv1042c', 'IG_Rv1042c_Rv1043c', 'IG_Rv1046c_Rv1047', 'Rv1047', 'IG_Rv1047_Rv1048c', 'IG_Rv1053c_Rv1054', 'Rv1054', 'IG_Rv1066_Rv1067c', 'Rv1067c', 'IG_Rv1067c_Rv1068c', 'Rv1068c', 'IG_Rv1068c_Rv1069c', 'IG_Rv1086_Rv1087', 'Rv1087', 'IG_Rv1087_Rv1087A', 'IG_Rv1087A_Rv1088', 'Rv1088', 'Rv1089', 'IG_Rv1089_Rv1089A', 'IG_Rv1090_Rv1091', 'Rv1091', 'IG_Rv1091_Rv1092c', 'IG_Rv1127c_Rv1128c', 'Rv1128c', 'IG_Rv1128c_Rv1129c', 'IG_Rv1134_Rv1135c', 'Rv1135c', 'IG_Rv1135c_Rv1135A', 'IG_Rv1147_Rv1148c', 'Rv1148c', 'IG_Rv1148c_Rv1149', 'Rv1149', 'IG_Rv1149_Rv1151c', 'IG_Rv1167c_Rv1168c', 'Rv1168c', 'IG_Rv1168c_Rv1169c', 'Rv1169c', 'IG_Rv1169c_Rv1170', 'IG_Rv1171_Rv1172c', 'Rv1172c', 'IG_Rv1172c_Rv1173', 'IG_Rv1194c_Rv1195', 'Rv1195', 'IG_Rv1195_Rv1196', 'Rv1196', 'IG_Rv1196_Rv1197', 'IG_Rv1198_Rv1199c', 'Rv1199c', 'IG_Rv1199c_Rv1200', 'IG_Rv1213_Rv1214c', 'Rv1214c', 'IG_Rv1214c_Rv1215c', 'IG_Rv1242_Rv1243c', 'Rv1243c', 'IG_Rv1243c_Rv1244', 'IG_Rv1312_Rv1313c', 'Rv1313c', 'IG_Rv1313c_Rv1314c', 'IG_Rv1324_Rv1325c', 'Rv1325c', 'IG_Rv1325c_Rv1326c', 'IG_Rv1360_Rv1361c', 'Rv1361c', 'IG_Rv1361c_Rv1362c', 'IG_Rv1368_Rv1369c', 'Rv1369c', 'IG_Rv1369c_Rv1370c', 'Rv1370c', 'IG_Rv1370c_Rv1371', 'IG_Rv1385_Rv1386', 'Rv1386', 'Rv1387', 'IG_Rv1387_Rv1388', 'IG_Rv1395_Rv1396c', 'Rv1396c', 'IG_Rv1396c_Rv1397c', 'IG_Rv1429_Rv1430', 'Rv1430', 'IG_Rv1430_Rv1431', 'IG_Rv1440_Rv1441c', 'Rv1441c', 'IG_Rv1441c_Rv1442', 'IG_Rv1449c_Rv1450c', 'Rv1450c', 'IG_Rv1450c_Rv1451', 'IG_Rv1451_Rv1452c', 'Rv1452c', 'IG_Rv1452c_Rv1453', 'IG_Rv1467c_Rv1468c', 'Rv1468c', 'IG_Rv1468c_Rv1469', 'IG_Rv1547_Rv1548c', 'Rv1548c', 'IG_Rv1548c_Rv1549', 'IG_Rv1571_Rv1572c', 'Rv1572c', 'IG_Rv1572c_Rv1573', 'Rv1573', 'IG_Rv1573_Rv1574', 'Rv1574', 'Rv1575', 'Rv1576c', 'IG_Rv1576c_Rv1577c', 'Rv1577c', 'IG_Rv1577c_Rv1578c', 'Rv1578c', 'IG_Rv1578c_Rv1579c', 'Rv1579c', 'Rv1580c', 'IG_Rv1580c_Rv1581c', 'Rv1581c', 'IG_Rv1581c_Rv1582c', 'Rv1582c', 'Rv1583c', 'Rv1584c', 'IG_Rv1584c_Rv1585c', 'Rv1585c', 'Rv1586c', 'Rv1587c', 'IG_Rv1587c_Rv1588c', 'Rv1588c', 'IG_Rv1588c_Rv1589', 'IG_Rv1645c_Rv1646', 'Rv1646', 'IG_Rv1646_Rv1647', 'IG_Rv1650_Rv1651c', 'Rv1651c', 'IG_Rv1651c_Rv1652', 'IG_Rv1704c_Rv1705c', 'Rv1705c', 'IG_Rv1705c_Rv1706c', 'Rv1706c', 'IG_Rv1706c_Rv1706A', 'IG_Rv1752_Rv1753c', 'Rv1753c', 'IG_Rv1753c_Rv1754c', 'IG_Rv1755c_Rv1756c', 'Rv1756c', 'IG_Rv1756c_Rv1757c', 'Rv1757c', 'IG_Rv1757c_Rv1758', 'IG_Rv1758_Rv1759c', 'Rv1759c', 'IG_Rv1759c_Rv1760', 'IG_Rv1762c_Rv1763', 'Rv1763', 'IG_Rv1763_Rv1764', 'Rv1764', 'IG_Rv1764_Rv1765c', 'IG_Rv1765c_Rv1765A', 'Rv1765A', 'IG_Rv1765A_Rv1766', 'IG_Rv1767_Rv1768', 'Rv1768', 'IG_Rv1768_Rv1769', 'IG_Rv1786_Rv1787', 'Rv1787', 'IG_Rv1787_Rv1788', 'Rv1788', 'IG_Rv1788_Rv1789', 'Rv1789', 'IG_Rv1789_Rv1790', 'Rv1790', 'IG_Rv1790_Rv1791', 'Rv1791', 'IG_Rv1791_Rv1793', 'IG_Rv1799_Rv1800', 'Rv1800', 'IG_Rv1800_Rv1801', 'Rv1801', 'IG_Rv1801_Rv1802', 'Rv1802', 'IG_Rv1802_Rv1803c', 'Rv1803c', 'IG_Rv1803c_Rv1804c', 'IG_Rv1805c_Rv1806', 'Rv1806', 'IG_Rv1806_Rv1807', 'Rv1807', 'IG_Rv1807_Rv1808', 'Rv1808', 'IG_Rv1808_Rv1809', 'Rv1809', 'IG_Rv1809_Rv1810', 'IG_Rv1817_Rv1818c', 'Rv1818c', 'IG_Rv1818c_Rv1819c', 'IG_Rv1839c_Rv1840c', 'Rv1840c', 'IG_Rv1840c_Rv1841c', 'IG_Rv1916_Rv1917c', 'Rv1917c', 'IG_Rv1917c_Rv1918c', 'Rv1918c', 'IG_Rv1918c_Rv1919c', 'IG_Rv1944c_Rv1945', 'Rv1945', 'IG_Rv1945_Rv1946c', 'IG_Rv1982c_Rv1983', 'Rv1983', 'IG_Rv2012_Rv2013', 'Rv2013', 'Rv2014', 'IG_Rv2014_Rv2015c', 'IG_Rv2097c_Rv2100', 'Rv2100', 'IG_Rv2100_Rv2101', 'IG_Rv2104c_Rv2105', 'Rv2105', 'Rv2106', 'IG_Rv2106_Rv2107', 'Rv2107', 'IG_Rv2107_Rv2108', 'Rv2108', 'IG_Rv2108_Rv2109c', 'IG_Rv2122c_Rv2123', 'Rv2123', 'IG_Rv2125_Rv2126c', 'Rv2126c', 'IG_Rv2126c_Rv2127', 'IG_Rv2161c_Rv2162c', 'Rv2162c', 'IG_Rv2162c_Rv2163c', 'IG_Rv2166c_Rv2167c', 'Rv2167c', 'Rv2168c', 'IG_Rv2168c_Rv2169c', 'IG_Rv2176_Rv2177c', 'Rv2177c', 'IG_Rv2177c_Rv2178c', 'IG_Rv2277c_Rv2278', 'Rv2278', 'Rv2279', 'IG_Rv2279_Rv2280', 'IG_Rv2327_Rv2328', 'Rv2328', 'IG_Rv2328_Rv2329c', 'IG_Rv2339_Rv2340c', 'Rv2340c', 'IG_Rv2340c_Rv2341', 'IG_Rv2351c_Rv2352c', 'Rv2352c', 'IG_Rv2352c_Rv2353c', 'Rv2353c', 'IG_Rv2353c_Rv2354', 'Rv2354', 'Rv2355', 'IG_Rv2355_Rv2356c', 'Rv2356c', 'IG_Rv2356c_Rv2357c', 'IG_Rv2370c_Rv2371', 'Rv2371', 'IG_Rv2371_Rv2372c', 'IG_Rv2395_Rv2396', 'Rv2396', 'IG_Rv2396_Rv2397c', 'IG_Rv2407_Rv2408', 'Rv2408', 'IG_Rv2423_Rv2424c', 'Rv2424c', 'IG_Rv2424c_Rv2425c', 'Rv2430c', 'IG_Rv2430c_Rv2431c', 'Rv2431c', 'IG_Rv2431c_Rv2432c', 'IG_Rv2478c_Rv2479c', 'Rv2479c', 'Rv2480c', 'IG_Rv2480c_Rv2481c', 'IG_Rv2486_Rv2487c', 'Rv2487c', 'IG_Rv2487c_Rv2488c', 'IG_Rv2489c_Rv2490c', 'Rv2490c', 'IG_Rv2490c_Rv2491', 'IG_Rv2511_Rv2512c', 'Rv2512c', 'IG_Rv2512c_Rv2513', 'IG_Rv2518c_Rv2519', 'Rv2519', 'IG_Rv2519_Rv2520c', 'IG_Rv2590_Rv2591', 'Rv2591', 'IG_Rv2591_Rv2592c', 'IG_Rv2607_Rv2608', 'Rv2608', 'IG_Rv2608_Rv2609c', 'Rv2615c', 'IG_Rv2615c_Rv2616', 'IG_Rv2633c_Rv2634c', 'Rv2634c', 'IG_Rv2634c_Rv2635', 'Rv2646', 'IG_Rv2646_Rv2647', 'IG_Rv2647_Rv2648', 'Rv2648', 'Rv2649', 'IG_Rv2649_Rv2650c', 'Rv2650c', 'IG_Rv2650c_Rv2651c', 'Rv2651c', 'IG_Rv2651c_Rv2652c', 'Rv2652c', 'IG_Rv2652c_Rv2653c', 'Rv2653c', 'IG_Rv2653c_Rv2654c', 'Rv2654c', 'Rv2655c', 'Rv2656c', 'Rv2657c', 'IG_Rv2657c_Rv2658c', 'Rv2658c', 'IG_Rv2658c_Rv2659c', 'Rv2659c', 'IG_Rv2659c_Rv2660c', 'IG_Rv2665_Rv2666', 'Rv2666', 'IG_Rv2666_Rv2667', 'IG_Rv2740_Rv2741', 'Rv2741', 'IG_Rv2741_Rv2742c', 'IG_Rv2767c_Rv2768c', 'Rv2768c', 'IG_Rv2768c_Rv2769c', 'Rv2769c', 'IG_Rv2769c_Rv2770c', 'Rv2770c', 'IG_Rv2770c_Rv2771c', 'IG_Rv2790c_Rv2791c', 'Rv2791c', 'IG_Rv2809_Rv2810c', 'Rv2810c', 'IG_Rv2811_Rv2812', 'Rv2812', 'IG_Rv2813_Rv2814c', 'Rv2814c', 'Rv2815c', 'IG_Rv2815c_Rv2816c', 'IG_Rv2852c_Rv2853', 'Rv2853', 'IG_Rv2853_Rv2854', 'IG_Rv2884_Rv2885c', 'Rv2885c', 'Rv2892c', 'IG_Rv2892c_Rv2893', 'IG_Rv2942_Rv2943', 'Rv2943', 'Rv2943A', 'Rv2944', 'IG_Rv2944_Rv2945c', 'IG_Rv2960c_Rv2961', 'Rv2961', 'IG_Rv2961_Rv2962c', 'Rv2978c', 'IG_Rv3017c_Rv3018c', 'Rv3018c', 'IG_Rv3018c_Rv3018A', 'Rv3018A', 'IG_Rv3018A_Rv3019c', 'IG_Rv3020c_Rv3021c', 'Rv3021c', 'Rv3022c', 'Rv3022A', 'IG_Rv3022A_Rv3023c', 'Rv3023c', 'IG_Rv3023c_Rv3024c', 'IG_Rv3096_Rv3097c', 'Rv3097c', 'IG_Rv3097c_Rv3098c', 'IG_Rv3114_Rv3115', 'Rv3115', 'IG_Rv3115_Rv3116', 'IG_Rv3124_Rv3125c', 'Rv3125c', 'IG_Rv3125c_Rv3126c', 'IG_Rv3134c_Rv3135', 'Rv3135', 'IG_Rv3135_Rv3136', 'Rv3136', 'IG_Rv3136_Rv3137', 'IG_Rv3143_Rv3144c', 'Rv3144c', 'IG_Rv3144c_Rv3145', 'IG_Rv3158_Rv3159c', 'Rv3159c', 'IG_Rv3159c_Rv3160c', 'IG_Rv3183_Rv3184', 'Rv3184', 'Rv3185', 'IG_Rv3185_Rv3186', 'Rv3186', 'Rv3187', 'IG_Rv3187_Rv3188', 'IG_Rv3190c_Rv3191c', 'Rv3191c', 'IG_Rv3191c_Rv3192', 'IG_Rv3324c_Rv3325', 'Rv3325', 'Rv3326', 'IG_Rv3326_Rv3327', 'Rv3327', 'IG_Rv3342_Rv3343c', 'Rv3343c', 'IG_Rv3343c_Rv3344c', 'Rv3344c', 'Rv3345c', 'IG_Rv3345c_Rv3346c', 'IG_Rv3346c_Rv3347c', 'Rv3347c', 'IG_Rv3347c_Rv3348', 'Rv3348', 'IG_Rv3348_Rv3349c', 'Rv3349c', 'IG_Rv3349c_Rv3350c', 'Rv3350c', 'IG_Rv3350c_Rv3351c', 'IG_Rv3366_Rv3367', 'Rv3367', 'IG_Rv3379c_Rv3380c', 'Rv3380c', 'IG_Rv3380c_Rv3381c', 'Rv3381c', 'IG_Rv3381c_Rv3382c', 'IG_Rv3385c_Rv3386', 'Rv3386', 'Rv3387', 'IG_Rv3387_Rv3388', 'Rv3388', 'IG_Rv3388_Rv3389c', 'IG_Rv3424c_Rv3425', 'Rv3425', 'IG_Rv3425_Rv3426', 'Rv3426', 'IG_Rv3426_Rv3427c', 'Rv3427c', 'IG_Rv3427c_Rv3428c', 'Rv3428c', 'IG_Rv3428c_Rv3429', 'Rv3429', 'IG_Rv3465_Rv3466', 'Rv3466', 'Rv3467', 'IG_Rv3467_Rv3468c', 'IG_Rv3473c_Rv3474', 'Rv3474', 'Rv3475', 'IG_Rv3475_Rv3476c', 'IG_Rv3476c_Rv3477', 'Rv3477', 'IG_Rv3477_Rv3478', 'Rv3478', 'IG_Rv3478_Rv3479', 'IG_Rv3506_Rv3507', 'Rv3507', 'IG_Rv3507_Rv3508', 'Rv3508', 'IG_Rv3508_Rv3509c', 'IG_Rv3510c_Rv3511', 'Rv3511', 'Rv3512', 'IG_Rv3512_Rv3513c', 'IG_Rv3513c_Rv3514', 'Rv3514', 'IG_Rv3514_Rv3515c', 'IG_Rv3531c_Rv3532', 'Rv3532', 'IG_Rv3532_Rv3533c', 'Rv3533c', 'IG_Rv3533c_Rv3534c', 'IG_Rv3538_Rv3539', 'Rv3539', 'IG_Rv3557c_Rv3558', 'Rv3558', 'IG_Rv3558_Rv3559c', 'Rv3590c', 'IG_Rv3590c_Rv3591c', 'IG_Rv3594_Rv3595c', 'Rv3595c', 'IG_Rv3595c_Rv3596c', 'IG_Rv3620c_Rv3621c', 'Rv3621c', 'IG_Rv3621c_Rv3622c', 'Rv3622c', 'IG_Rv3622c_Rv3623', 'IG_Rv3635_Rv3636', 'Rv3636', 'IG_Rv3636_Rv3637', 'Rv3637', 'Rv3638', 'IG_Rv3638_Rv3639c', 'IG_Rv3649_Rv3650', 'Rv3650', 'IG_Rv3650_Rv3651', 'IG_Rv3651_Rv3652', 'Rv3652', 'Rv3653', 'IG_Rv3653_Rv3654c', 'Rv3738c', 'IG_Rv3738c_Rv3739c', 'Rv3739c', 'IG_Rv3739c_Rv3740c', 'IG_Rv3745c_Rv3746c', 'Rv3746c', 'IG_Rv3746c_Rv3747', 'IG_Rv3750c_Rv3751', 'Rv3751', 'IG_Rv3751_Rv3752c', 'IG_Rv3797_Rv3798', 'Rv3798', 'IG_Rv3798_Rv3799c', 'IG_Rv3811_Rv3812', 'Rv3812', 'IG_Rv3812_Rv3813c', 'Rv3827c', 'IG_Rv3843c_Rv3844', 'Rv3844', 'IG_Rv3844_Rv3845', 'IG_Rv3871_Rv3872', 'Rv3872', 'IG_Rv3872_Rv3873', 'Rv3873', 'IG_Rv3873_Rv3874', 'IG_Rv3891c_Rv3892c', 'Rv3892c', 'IG_Rv3892c_Rv3893c', 'Rv3893c', 'IG_Rv3893c_Rv3894c']

ref_snps = []

with open("ref_snps.txt") as ref:
	for line in ref:
		ref_snps.append(line.strip())

phyresse = ['Rv0005','Rv0006','Rv0486','Rv0667','Rv0682','Rv0701','Rv1483','Rv1484','Rv1630','Rv1694','Rv1854c','Rv1908c','Rv2043c','Rv2416c','Rv2428','Rv2764c','Rv3793','Rv3794','Rv3795','Rv3854c','Rv3919c']
resistance = {'2288782TC': 'pyrazinamide', '4248747GA': 'ethambutol', '2289038CG': 'pyrazinamide', '6735AC': 'fluoroquinolones', '7570CT': 'fluoroquinolones', '2289038CA': 'pyrazinamide', '2289111AC': 'pyrazinamide', '2289068GC': 'pyrazinamide', '2155206GC': 'isoniazidK', '2288740TG': 'pyrazinamide', '2289068GT': 'pyrazinamide', '2288727AG': 'pyrazinamide', '2288853AC': 'pyrazinamide', '2288853AG': 'pyrazinamide', '2288828AC': 'pyrazinamide', '1472751AG': 'streptomycin', '2288957GC': 'pyrazinamide', '761139CA': 'rifampicin', '2289081GT': 'pyrazinamide', '761139CG': 'rifampicin', '4247863CG': 'ethambutol', '2289219TC': 'pyrazinamide', '761108GT': 'rifampicin', '761139CT': 'rifampicin', '2289081GC': 'pyrazinamide', '2289081GA': 'pyrazinamide', '4407790GA': 'streptomycin', '2289073GA': 'pyrazinamide', '2289073GC': 'pyrazinamide', '2289234GT': 'pyrazinamide', '4407824GA': 'streptomycin', '2289203GC': 'pyrazinamide', '7582AT': 'fluoroquinolones', '781822AG': 'streptomycin', '2288887AC': 'pyrazinamide', '2288887AG': 'pyrazinamide', '2288697AG': 'pyrazinamide', '4247429AC': 'ethambutol', '4247429AG': 'ethambutol', '2289096TG': 'pyrazinamide', '2289096TC': 'pyrazinamide', '2289000AG': 'pyrazinamide', '2288885CT': 'pyrazinamide', '2289000AC': 'pyrazinamide', '4408102CG': 'streptomycin', '2288961AG': 'pyrazinamide', '2288839TG': 'pyrazinamide', '2288961AC': 'pyrazinamide', '1834325GA': 'pyrazinamide', '2288841GA': 'pyrazinamide', '2289043AG': 'pyrazinamide', '4408009AC': 'streptomycin', '4247513TC': 'ethambutol', '4247431GT': 'ethambutol', '2715346GA': 'kanamycin', '4247430TC': 'ethambutol', '2155169TC': 'isoniazidK', '4247431GC': 'ethambutol', '4247431GA': 'ethambutol', '1472359AC': 'streptomycin', '2289061TG': 'pyrazinamide', '1674782TC': 'isoniazidO', '2289036GA': 'pyrazinamide', '2289235CG': 'pyrazinamide', '7582AC': 'fluoroquinolones', '1918388TC': 'capreomycin', '7582AG': 'fluoroquinolones', '1673424AG': 'isoniazidO', '1917979CT': 'capreomycin', '2288997TC': 'pyrazinamide', '2155167GC': 'isoniazidO', '2288928CT': 'pyrazinamide', '2288886CG': 'pyrazinamide', '2155167GT': 'isoniazidK', '2288886CT': 'pyrazinamide', '4407604GT': 'streptomycin', '4247469AC': 'ethambutol', '761110AG': 'rifampicin', '2288857CA': 'pyrazinamide', '761109GT': 'rifampicin', '2289052AC': 'pyrazinamide', '2288938CT': 'pyrazinamide', '761110AT': 'rifampicin', '2155214AC': 'isoniazidK', '1918211CA': 'capreomycin', '2289193CT': 'pyrazinamide', '2288703AC': 'pyrazinamide', '2288703AG': 'pyrazinamide', '1918292TC': 'capreomycin', '2289208CT': 'pyrazinamide', '7581GT': 'fluoroquinolones', '4246734TG': 'ethambutol', '7581GC': 'fluoroquinolones', '7581GA': 'fluoroquinolones', '2289218GT': 'pyrazinamide', '2288683AG': 'pyrazinamide', '4244193GA': 'ethambutol', '2288935AC': 'pyrazinamide', '2288848CA': 'pyrazinamide', '761140AC': 'rifampicin', '761140AG': 'rifampicin', '1673425CT': 'isoniazidO', '2288838GT': 'pyrazinamide', '2288874CG': 'pyrazinamide', '761140AT': 'rifampicin', '2288806CT': 'pyrazinamide', '4247729GA': 'ethambutol', '2289219TG': 'pyrazinamide', '2289159GT': 'pyrazinamide', '2288806CG': 'pyrazinamide', '4247729GT': 'ethambutol', '2288902TG': 'pyrazinamide', '2288719TC': 'pyrazinamide', '2288847CG': 'pyrazinamide', '1473247CT': 'ami/kana/capreomycin', '2288805GT': 'pyrazinamide', '2289040AG': 'pyrazinamide', '2288847CT': 'pyrazinamide', '2289082GA': 'pyrazinamide', '2288944TC': 'pyrazinamide', '2288944TG': 'pyrazinamide', '2288868AC': 'pyrazinamide', '2289133CA': 'pyrazinamide', '2288704CA': 'pyrazinamide', '2289090TC': 'pyrazinamide', '760314GT': 'rifampicin', '6620GA': 'fluoroquinolones', '6620GC': 'fluoroquinolones', '2288844AG': 'pyrazinamide', '1918489CT': 'capreomycin', '6749GA': 'fluoroquinolones', '6737AC': 'fluoroquinolones', '2289090TG': 'pyrazinamide', '2288805GA': 'pyrazinamide', '6621AC': 'fluoroquinolones', '4245730AC': 'ethambutol', '1918489CA': 'capreomycin', '2288988AC': 'pyrazinamide', '2288988AG': 'pyrazinamide', '6741AT': 'fluoroquinolones', '7566GA': 'fluoroquinolones', '2289239CT': 'pyrazinamide', '2289239CA': 'pyrazinamide', '761154TG': 'rifampicin', '2155168CT': 'isoniazidK', '1918139CA': 'capreomycin', '1475956GT': 'linezolid', '4247573GA': 'ethambutol', '2288920CT': 'pyrazinamide', '2288931CA': 'pyrazinamide', '2289039CG': 'pyrazinamide', '4243245GA': 'ethambutol', '2289231AC': 'pyrazinamide', '4243221CT': 'ethambutol', '2288764TG': 'pyrazinamide', '2289231AG': 'pyrazinamide', '2155699TC': 'isoniazidK', '2289089GT': 'pyrazinamide', '2289039CT': 'pyrazinamide', '2288718AG': 'pyrazinamide', '1918487CT': 'capreomycin', '2289030TC': 'pyrazinamide', '4247723CT': 'ethambutol', '781687AG': 'streptomycin', '4243225CA': 'ethambutol', '2288952CT': 'pyrazinamide', '2288920CG': 'pyrazinamide', '2288956TG': 'pyrazinamide', '2289223CA': 'pyrazinamide', '2288956TC': 'pyrazinamide', '2289248AG': 'pyrazinamide', '1917946CT': 'capreomycin', '2288952CG': 'pyrazinamide', '1472752AT': 'streptomycin', '2288930GT': 'pyrazinamide', '1674481TG': 'isoniazidO', '2289057GA': 'pyrazinamide', '761100CA': 'rifampicin', '7564GC': 'fluoroquinolones', '6736CG': 'fluoroquinolones', '2288821GA': 'pyrazinamide', '1833909AC': 'pyrazinamide', '2289171CT': 'pyrazinamide', '2289138AG': 'pyrazinamide', '2289225AG': 'pyrazinamide', '2288757CT': 'pyrazinamide', '2288779CT': 'pyrazinamide', '4248002CA': 'ethambutol', '2288895AG': 'pyrazinamide', '2288895AC': 'pyrazinamide', '2288818TC': 'pyrazinamide', '2289054TC': 'pyrazinamide', '2289150AC': 'pyrazinamide', '761120CG': 'rifampicin', '3073808GC': 'para-aminosalicylic', '2289071GC': 'pyrazinamide', '7572TC': 'fluoroquinolones', '2289016TG': 'pyrazinamide', '2288754AG': 'pyrazinamide', '761161TC': 'rifampicin', '1918003CT': 'capreomycin', '1476471GT': 'linezolid', '2288830AG': 'pyrazinamide', '2288832TC': 'pyrazinamide', '2288832TG': 'pyrazinamide', '2289072TC': 'pyrazinamide', '2289186AG': 'pyrazinamide', '2289072TG': 'pyrazinamide', '2288934TG': 'pyrazinamide', '2288934TC': 'pyrazinamide', '781821AC': 'streptomycin', '2288827CG': 'pyrazinamide', '1673432TC': 'isoniazidO', '1673432TA': 'isoniazidO', '4407992CT': 'streptomycin', '1918144AG': 'capreomycin', '2289103TC': 'pyrazinamide', '2289103TG': 'pyrazinamide', '7563GT': 'fluoroquinolones', '2289252TA': 'pyrazinamide', '2289252TC': 'pyrazinamide', '2289252TG': 'pyrazinamide', '1472750CA': 'streptomycin', '2288820TG': 'pyrazinamide', '1918494TG': 'capreomycin', '2288953CT': 'pyrazinamide', '2289028AG': 'pyrazinamide', '2288962AG': 'pyrazinamide', '2155222CA': 'isoniazidK', '2155289TG': 'isoniazidK', '2289162AG': 'pyrazinamide', '2289201CT': 'pyrazinamide', '2288817GT': 'pyrazinamide', '6734AG': 'fluoroquinolones', '2289108AC': 'pyrazinamide', '2289180AC': 'pyrazinamide', '2288730GA': 'pyrazinamide', '4247873GA': 'ethambutol', '761095TC': 'rifampicin', '761095TG': 'rifampicin', '4247507TC': 'ethambutol', '1673423GT': 'isoniazidO', '2288998GC': 'pyrazinamide', '2288836CT': 'pyrazinamide', '4407940AG': 'streptomycin', '4243242GA': 'ethambutol', '2289040AC': 'pyrazinamide', '4247730GA': 'ethambutol', '4247730GC': 'ethambutol', '2288848CT': 'pyrazinamide', '4243833GA': 'ethambutol', '761111CG': 'rifampicin', '2289204AG': 'pyrazinamide', '2289029AT': 'pyrazinamide', '2288833GC': 'pyrazinamide', '6742AT': 'fluoroquinolones', '2155168CG': 'isoniazidK', '4244617CT': 'ethambutol', '2155168CA': 'isoniazidK', '2288982GA': 'pyrazinamide', '2289206GC': 'pyrazinamide', '6738CA': 'fluoroquinolones', '1472362CT': 'streptomycin', '761101AT': 'rifampicin', '761101AC': 'rifampicin', '4249518AG': 'ethambutol', '1918651GA': 'capreomycin', '2288960GT': 'pyrazinamide', '2288778AC': 'pyrazinamide', '2288960GC': 'pyrazinamide', '2289214GT': 'pyrazinamide', '1472358CT': 'streptomycin', '2288880CG': 'pyrazinamide', '2288955TG': 'pyrazinamide', '2288955TC': 'pyrazinamide', '2288836CA': 'pyrazinamide', '801268TC': 'linezolid', '4248003AG': 'ethambutol', '2289200GT': 'pyrazinamide', '2289220CT': 'pyrazinamide', '761098GC': 'rifampicin', '4407931AG': 'streptomycin', '2715342CT': 'kanamycin', '761098GT': 'rifampicin', '2288752AG': 'pyrazinamide', '2289001AC': 'pyrazinamide', '6750CT': 'fluoroquinolones', '2288869CA': 'pyrazinamide', '2289097CT': 'pyrazinamide', '2289140GC': 'pyrazinamide', '2289202AG': 'pyrazinamide', '4247495GT': 'ethambutol', '2288859AC': 'pyrazinamide', '761128CT': 'rifampicin', '761155CT': 'rifampicin', '2288933GC': 'pyrazinamide', '2289070AG': 'pyrazinamide', '761128CG': 'rifampicin', '2288971CA': 'pyrazinamide', '2289216AG': 'pyrazinamide', '2288945GT': 'pyrazinamide', '2289216AC': 'pyrazinamide', '2289100TC': 'pyrazinamide', '2102715TC': 'isoniazidO', '2289100TA': 'pyrazinamide', '2102240CT': 'isoniazidO', '2289050AC': 'pyrazinamide', '2289222AC': 'pyrazinamide', '1473246AG': 'ami/kana/capreomycin', '6575CT': 'fluoroquinolones', '2289222AT': 'pyrazinamide', '4247402TG': 'ethambutol', '761093GC': 'rifampicin', '4247717CG': 'ethambutol', '2288826AC': 'pyrazinamide', '1917991CT': 'capreomycin', '4244281GA': 'ethambutol', '2289213TG': 'pyrazinamide', '2289213TC': 'pyrazinamide', '2288766AC': 'pyrazinamide', '2288883AT': 'pyrazinamide', '761277AT': 'rifampicin', '2155212CG': 'isoniazidK', '2289207TG': 'pyrazinamide', '2288883AG': 'pyrazinamide', '2288883AC': 'pyrazinamide', '1473329GT': 'ami/kana/capreomycin', '2288772AC': 'pyrazinamide', '2288772AG': 'pyrazinamide', '2288823CG': 'pyrazinamide', '761004AG': 'rifampicin', '2289031GA': 'pyrazinamide', '761141CA': 'rifampicin', '2288817GA': 'pyrazinamide', '2726136CT': 'isoniazidO', '2289091GA': 'pyrazinamide', '4241078AG': 'ethambutol', '2289240AT': 'pyrazinamide', '4247496AG': 'ethambutol', '2289240AG': 'pyrazinamide', '2288761CG': 'pyrazinamide', '1918322TA': 'capreomycin', '2726145GA': 'isoniazidO', '2288954CT': 'pyrazinamide', '761155CG': 'rifampicin', '2289248AC': 'pyrazinamide'}

code = {"UUU":"F", "UUC":"F", "UUA":"L", "UUG":"L",
    "UCU":"S", "UCC":"S", "UCA":"S", "UCG":"S",
    "UAU":"Y", "UAC":"Y", "UAA":"STOP", "UAG":"STOP",
    "UGU":"C", "UGC":"C", "UGA":"STOP", "UGG":"W",
    "CUU":"L", "CUC":"L", "CUA":"L", "CUG":"L",
    "CCU":"P", "CCC":"P", "CCA":"P", "CCG":"P",
    "CAU":"H", "CAC":"H", "CAA":"Q", "CAG":"Q",
    "CGU":"R", "CGC":"R", "CGA":"R", "CGG":"R",
    "AUU":"I", "AUC":"I", "AUA":"I", "AUG":"M",
    "ACU":"T", "ACC":"T", "ACA":"T", "ACG":"T",
    "AAU":"N", "AAC":"N", "AAA":"K", "AAG":"K",
    "AGU":"S", "AGC":"S", "AGA":"R", "AGG":"R",
    "GUU":"V", "GUC":"V", "GUA":"V", "GUG":"V",
    "GCU":"A", "GCC":"A", "GCA":"A", "GCG":"A",
    "GAU":"D", "GAC":"D", "GAA":"E", "GAG":"E",
    "GGU":"G", "GGC":"G", "GGA":"G", "GGG":"G"}

def revComplement(seq): 
	baseComplement = {'A': 'U', 'C': 'G', 'G': 'C', 'U': 'A'} 
	letters = list(seq) 
	letters = [baseComplement[base] for base in letters] 
	temp = ''.join(letters)
	return(temp[::-1])

genes = {}

with open("annotation_revised.csv") as inAnnot:
	for line in inAnnot:
		line = line.strip().split(",")
		genes[line[0]] = [int(line[-3]),int(line[-2]),line[-1]]


def isSyn(snp_id):
	snp_id = snp_id.replace("T","U")
	if snp_id[-1] in ["A","U","C","G"] and snp_id[-2] in ["A","U","C","G"]:
		snp_pos = snp_id[:-2]
		this_gene = ""
		strand = ""
		this_seq = ""
		true_pos = ""

		for gene, coords in genes.items():
			if coords[0] <= int(snp_pos) <= coords[1]:
				this_gene = gene
				strand = coords[2]
				this_seq = ancestor[int(coords[0])-1:int(coords[1])]
				true_pos = int(snp_pos) - coords[0]
		
		if this_gene.startswith("Rv"):
			
			codons = [this_seq[i:i+3] for i in range(0, len(this_seq), 3)]
			this_codon = codons[int(float(true_pos) / 3)]
			codon_pos = int(float(true_pos) % 3)
			
			if strand == "-":
				this_codon = revComplement(this_codon.replace("T","U"))
			
				mutated_codon = list(this_codon)
				mutated_codon[abs(2-codon_pos)] = revComplement(snp_id[-1])
				mutated_codon = "".join(mutated_codon)
				if code[this_codon] == code[mutated_codon]:
					return "syn"
				else:
					return "non-syn"
			else:
			
				mutated_codon = list(this_codon)
				mutated_codon[codon_pos] = snp_id[-1]
				mutated_codon = "".join(mutated_codon)

				if code[this_codon.replace("T","U")] == code[mutated_codon.replace("T","U")]:
					return "syn"
				else:
					return "non-syn"
		else:
			return "intergenic"
	else:
		return "indet"


F0, F1 = {}, {}
aSnps = []

i = 0
for filename in patients[sys.argv[1]]:
	with open(filename+".snp") as snpfile:
		next(snpfile)
		for line in snpfile:
			line = line.strip().split("\t")

			snpid = line[1] + line[2] + line[3]

			freq = float(line[4].split(":")[4].replace("%",""))
			#freq = float(line[6].replace("%",""))

			if sys.argv[1] == "G039":
				freq = float(line[4].split(":")[4].replace("%",""))

			if freq >= 3:
				aSnps.append(snpid)

				if i == 0:
					F0[snpid] = freq
				elif i == 1:
					F1[snpid] = freq

	i += 1


fSnps = list(set(aSnps))

print "SNP_ID,Gene,Ann,F0,F1,Diff,in_Ref,DR,AB,Dyn"

for item in fSnps:

	freqs = []
	if item in F0.keys():
		freqs.append(F0[item])
	else:
		freqs.append(0)

	if item in F1.keys():
		freqs.append(F1[item])
	else:
		freqs.append(0)

	c1 = "-"

	if freqs[0] != "-" and freqs[1] == "-":
		c1 = "loss"
	if freqs[0] == "-" and freqs[1] != "-":
		c1 = "gain"
	if freqs[0] != "-" and freqs[1] != "-":
		c1 = "stable"
	if freqs[0] == "-" and freqs[1] == "-":
		c1 = "absent"


	ref = "no"
	if item[:-2] in ref_snps:
		ref = "yes"


	this_gene = ""
	for gene, coords in genes.items():
		if coords[0] <= int(item[:-2]) <= coords[1]:
			this_gene = gene

	call = isSyn(item)

	cat = "-"
	if this_gene in phyresse:
		cat = "DR"

	AB = "-"
	if item in resistance.keys():
		AB = resistance[item]

	if this_gene not in discard:
		print ",".join([item, this_gene, call, str(freqs[0]), str(freqs[1]), str(abs(freqs[0]-freqs[1])) ,ref, cat, AB, c1])

