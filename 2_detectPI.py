#!/usr/bin/env python

def parseArgs():

	import argparse

	parser = argparse.ArgumentParser(description="Script to detect polyclonal infections")
	parser.add_argument('-c', action="store_true", default=False, help="Calculate distances")
	parser.add_argument('-d', action="store_true", default=False, help="Detect polyclonal infections (distances must be already calculated)")
	parser.add_argument('-p', action="store", dest="phylogeny", help="Tree in Newick format with Hamming distances")
	parser.add_argument('-t', action="store", dest="threshold", help="Maximum distance", default=20)
	args = parser.parse_args()

	return args

def loadTree(treeFile):

	from ete3 import Tree
	t = Tree(treeFile)

	n = 0
	for leaf in t:
		n+=1

	if n > 1:
		print "Tree successfully loaded with", n, "leaves."
	else:
		print "Please provide a valid Newick tree with Hamming distances. Use -h for more info on how to run this script."

	return [t, n]

def calculateDistances(phylo, nl, filename):

	from progress.bar import IncrementalBar

	bar = IncrementalBar('Computing distances between leaves...', max=nl)

	distances = {}

	with open(filename.split(".")[0]+".dist", "w") as output:

		output.write("Strain1 Dist Strain2\n")

		for leaf in phylo.iter_leaves():
			distances[leaf.name] = []
			for leaf2 in phylo.iter_leaves():
				if leaf != leaf2:
					output.write(" ".join([leaf.name, str(phylo.get_distance(leaf, leaf2)), leaf2.name+"\n"]))
			bar.next()

		bar.finish()
		print "Distances saved to file:", filename.split(".")[0]+".dist"


def detectMI(phylo, distFile, threshold):

	distances = {}

	with open(distFile) as infile:

		next(infile)
		for line in infile:
			line = line.strip().split()
			if line[0] not in distances.keys():
				distances[line[0]] = []
				distances[line[0]].append(float(line[1]))
			else:
				distances[line[0]].append(float(line[1]))

	print "Now detecting likely polyclonal infections...\n"
	print "Leaf Closest_dist"

	for leaf in phylo.iter_leaves():

			if leaf.dist < 1.0:
				if min(distances[leaf.name]) >= float(threshold):
					print leaf.name, min(distances[leaf.name])


def main():

	args = vars(parseArgs())

	phylo = loadTree(args["phylogeny"])

	if args["c"] == True:
		calculateDistances(phylo[0], phylo[1], args["phylogeny"])
	
	if args["d"] == True:
		detectMI(phylo[0], args["phylogeny"].split(".")[0]+".dist",args["threshold"])

if __name__ == "__main__":
	main()
